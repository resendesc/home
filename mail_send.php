<?php

function pegaValor($valor) {
    return isset($_POST[$valor]) ? $_POST[$valor] : '';
}

function validaEmail($email) {
    return filter_var($email, FILTER_VALIDATE_EMAIL);
}

function enviaEmail($de, $assunto, $nome, $mensagem, $para, $email_servidor) {
    $headers = "From: $email_servidor\r\n" .
               "Reply-To: $de\r\n" .
               "X-Mailer: PHP/" . phpversion() . "\r\n";
    $headers .= "MIME-Version: 1.0\r\n";
    $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
  
  mail($para, $assunto, nl2br("Enviado por: ".$nome."<br />".$mensagem), $headers);
}

$email_servidor = "admin@infotecnologia.com.br";
$para = "resendesc@gmail.com";
$de = pegaValor("email");
$nome = pegaValor("nome");
$mensagem = pegaValor("mensagem");
$assunto = pegaValor("assunto");

if (validaEmail($de) && $mensagem) {
enviaEmail($de, $assunto, $nome, $mensagem, $para, $email_servidor);
$pagina = "mail_ok.php";
} else {
$pagina = "mail_error.php";
}

header("location:$pagina");

?>